const database = require('database');

const ID_NOT_FOUND = -1;

const QSearchPerson = '\
  SELECT id from sp_person \
  WHERE cpf = ?';

const QInsertPerson = '\
  INSERT INTO sp_person (cpf, name) \
  VALUES (?, ?)';

const QUpdatePerson = '\
  UPDATE sp_person \
  SET name = ? \
  WHERE id = ?';

const QInsertResponsable = '\
  INSERT INTO sp_responsible(username, personid) \
  VALUES (?, ?)';

const QUpdateResponsable = '\
  UPDATE sp_responsible \
    SET username = ? \
  WHERE id = ?';

const QSearchResponsable = '\
  SELECT id FROM sp_responsible \
  WHERE username = ? AND personid = ?';

const QSearchDiscipline = '\
  SELECT dr.id as disciplineRuleId \
    FROM sp_discipline d \
    INNER JOIN sp_discipline_rules dr ON ( \
      d.id = dr.disciplineid AND \
      d.unit_code = ? AND \
      d.discipline_code = ? AND \
      d.class_code = ? AND \
      dr.semester = ? AND \
      dr.active = 1 \
    )';

const QSearchResponsableRules = '\
  SELECT id \
    FROM sp_responsible_discipline_rules \
    WHERE responsibleid = ? AND disciplinerulesid = ?';

const QInsertResponsableRules = '\
  INSERT INTO sp_responsible_discipline_rules (responsibleid, disciplinerulesid) \
    VALUES (?, ?)';

const QInsertResponsableScore = '\
  INSERT INTO sp_responsable_disci_rule_score (responsiblediscruleid, scorewalletid) \
    VALUES (?, ?)';

/**
 * Formats Responsable data to be used on functions
 */
const formatResponsableData = (e, t) => ({
  cpf: e.cpf,
  username: e.username,
  name: e.name,
  unitCode: t.unit_id,
  disciplineCode: t.discipline_code,
  classCode: t.class_id,
  semester: t.semester
});

/**
 * Searchs for the disciplineRulesId
 */
const searchDiscipline = (conn, { unitCode, disciplineCode, classCode, semester }) => {
  return new Promise((resolve, reject) => {
    conn.query(QSearchDiscipline, [unitCode, disciplineCode, classCode, semester], (err, row) => {
      if (err) { return reject(err); }
      if (!row.length) {
        const column = 'unitCode/disciplineCode/classCode/semester';
        const params = `${unitCode}/${disciplineCode}/${classCode}/${semester}`;
        return reject(database.buildEntityNotFoundError('sp_discipline', column, params));
      }
      return resolve(row[0].disciplineRuleId);
    });
  });
};

/**
 * Searchs for the Responsable rules
 */
const searchResponsableRules = (conn, ResponsableId, disciplineRulesId, semester) => {
  return new Promise((resolve, reject) => {
    conn.query(QSearchResponsableRules, [ResponsableId, disciplineRulesId, semester], (err, row) => {
      if (err) { return reject(err); }
      if (!row.length) {
        return resolve(ID_NOT_FOUND);
      }
      return resolve(row[0].id);
    });
  });
};

/**
 * Inserts a Responsable Rules on database
 */
const insertResponsableRules = (conn, ResponsableId, disciplineRuleId) => {
  return new Promise((resolve, reject) => {
    conn.query(QInsertResponsableRules, [ResponsableId, disciplineRuleId], (err, result) => {
      if (err) { return reject(database.isIntegrityError(err).error); }
      return resolve(result.insertId);
    });
  });
};

/**
 * Inserts a Responsable Rules on database
 */
const insertResponsableScoreRelation = (conn, responsibleDiscRuleId, scoreWalletId) => {
  return new Promise((resolve, reject) => {
    conn.query(QInsertResponsableScore, [responsibleDiscRuleId, scoreWalletId], (err, result) => {
      if (err) { return reject(database.isIntegrityError(err).error); }
      return resolve(result.insertId);
    });
  });
};

/**
 * Searches a Person by CPF
 */
const searchPerson = (conn, cpf) => {
  return new Promise((resolve, reject) => {
    conn.query(QSearchPerson, [cpf], (err, result) => {
      if (err) { return reject(err); }
      if (result.length) { return resolve(result[0].id); }
      return resolve(ID_NOT_FOUND);
    });
  });
};

/**
 * Inserts a Person
 */
const insertPerson = (conn, cpf, name) => {
  return new Promise((resolve, reject) => {
    conn.query(QInsertPerson, [cpf, name], (err, row) => {
      if (err) { return reject(database.isIntegrityError(err).error); }
      return resolve(row.insertId);
    });
  });
};

/**
 * Updates a Person
 */
const updatePerson = (conn, id, name) => {
  return new Promise((resolve, reject) => {
    conn.query(QUpdatePerson, [name, id], (err) => {
      if (err) { return reject(database.isIntegrityError(err).error); }
      return resolve(id);
    });
  });
};

/**
 * Searches by a Responsable on database using his Ra, UnitCode, DisciplineCode and ClassCode
 */
const searchResponsable = (conn, username, personId) => {
  return new Promise((resolve, reject) => {
    conn.query(QSearchResponsable, [username, personId], (err, result) => {
      if (err) { return reject(err); }
      if (result.length) { return resolve(result[0].id); }
      return resolve(ID_NOT_FOUND);
    });
  });
};

/**
 * Inserts a Responsable on database
 */
const insertResponsable = (conn, username, personId) => {
  return new Promise((resolve, reject) => {
    conn.query(QInsertResponsable, [username, personId], (err, result) => {
      if (err) { return reject(database.isIntegrityError(err).error); }
      return resolve(result.insertId);
    });
  });
};

/**
 * Updates a Responsable on database
 */
const updateResponsable = (conn, id, username) => {
  return new Promise((resolve, reject) => {
    conn.query(QUpdateResponsable, [username, id], (err) => {
      if (err) { return reject(database.isIntegrityError(err).error); }
      return resolve(id);
    });
  });
};

/**
 * Saves a Responsable on database
 */
const saveResponsable = (responsableInfo) => {
  return new Promise((resolve, reject) => {
    let conn = null;
    let personId = null;
    let ResponsableId = null;
    let disciplineRuleId = null;

    database.getConnection()
      .then((connection) => {
        conn = connection;
        return searchPerson(conn, responsableInfo.cpf);
      })
      .then((rPersonId) => {
        if (rPersonId === ID_NOT_FOUND) {
          return insertPerson(conn, responsableInfo.cpf, responsableInfo.name);
        }
        return updatePerson(conn, rPersonId, responsableInfo.name);
      })
      .then((rPersonId) => {
        personId = rPersonId;
        return searchResponsable(conn, responsableInfo.username, personId);
      })
      .then((rResponsableId) => {
        if (rResponsableId === ID_NOT_FOUND) {
          return insertResponsable(conn, responsableInfo.username, personId);
        }
        return updateResponsable(conn, rResponsableId, responsableInfo.username);
      })
      .then((rResponsableId) => {
        ResponsableId = rResponsableId;
        return searchDiscipline(conn, responsableInfo);
      })
      .then((disciplineId) => {
        disciplineRuleId = disciplineId;
        return searchResponsableRules(conn, ResponsableId, disciplineRuleId, responsableInfo.semester);
      })
      .then((ResponsableRuleId) => {
        if (ResponsableRuleId === ID_NOT_FOUND) {
          return insertResponsableRules(conn, ResponsableId, disciplineRuleId);
        }
        return Promise.resolve(ResponsableRuleId);
      })
      .then((ResponsableRuleId) => {
        resolve(ResponsableRuleId);
      })
      .catch((err) => reject(err))
      .then(() => {
        if (conn) {
          conn.release();
        }
      });
  });
};


const saveResponsableGivenScore = (responsableId, scoreWalletId) => {
  return new Promise((resolve, reject) => {

    let conn = null;

    database.getConnection()
      .then((connection) => {
        conn = connection;
        return insertResponsableScoreRelation(connection, responsableId, scoreWalletId);
      })
      .then((responsibleScoreId) => {
        return resolve(responsibleScoreId);
      })
      .catch((err) => {
        return reject(err);
      })
      .then(() => {
        if (conn) {
          conn.release();
        }
      });
  })
}

module.exports.saveResponsableGivenScore = saveResponsableGivenScore;
module.exports.saveResponsable = saveResponsable;
module.exports.formatResponsableData = formatResponsableData;
